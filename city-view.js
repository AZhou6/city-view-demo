const accessKey = 'Ofa7l5iXePQfUluUtm5cClxWB2WM6Tq1IJqDV-qMxPM'
const baseURL = 'https://api.unsplash.com/'
const eleInput = document.querySelector('.inputCity')
const eleSearch = document.querySelector('.fa-search')
const user_input = 'user_input'
const currentIndex = 'currentIndex'
const currentPage = 'currentPage'
const currentImage = 'currentImage'
let imgContainer = document.getElementById('imgDiv')
let input_result = 'Toronto'
let groupImages = []
let groupDescription = []
let input_all_results = []
let eleBody = document.querySelector('body')
let eleDes = document.querySelector('.caption')
const clickedIndex = 'images'
const arrowLeft = document.getElementById("leftArrow")
const arrowRight = document.getElementById("rightArrow")
let pageNum=1
let totalPages=0
let match=false;


const searchFun = () => {

    match = true;
     input_result = eleInput.value.trim().toLowerCase()
    console.log("inputttt", input_result)
    pageNum =1

    input_all_results.push(input_result)
    localStorage.setItem(user_input, JSON.stringify(input_result))
    localStorage.setItem('user_input_all_history', JSON.stringify(input_all_results))
    localStorage.setItem(currentPage, JSON.stringify(1))
    fetchCity()


}
const inputSearch = (e) => {
    //console.log(e.key)
    e.key === 'Enter' && searchFun()
}

const fetchCity = () => {

    //console.log("checking", match)
    // Retrieve the user_input from local storage and make it the query
    let temp = localStorage.getItem(user_input)
    input_result = JSON.parse(temp)

    let newURL=`${baseURL}search/photos?client_id=${accessKey}&query=${input_result}&orientation=landscape&page=${pageNum}`
    //console.log("newwww",newURL)

    fetch(newURL)

        .then(response => response.json())
        //If there is data then retrieve whatever is below (promise fulfilled)
        .then(data => {

            console.log('data=====>', data)
            console.log("asd",data.results[0])
            console.log('asd2', data.results[1])
            console.log('asd3', data.results[2])

            if(data.results[0] === undefined){
                console.log("here")
                window.alert("error404")
                return
            }

            let check = JSON.parse(localStorage.getItem("groupOfImages"))
              let checkDesc = JSON.parse(localStorage.getItem('groupOfDescriptions'))
                //let pageNumber = JSON.parse(localStorage.getItem(currentPage))
            //sets the page number to the local one so that when we hit the arrow keys, the pages will stay consistent
            let localPN = JSON.parse(localStorage.getItem(currentPage))
            pageNum = localPN

            //Get the url & description of the images from local storage when the change pages.
            let newDataImage = check.map(function(item,index){
                return{
                    url: item,
                    des: checkDesc[index]
                }
            })
             //console.log("newDataImage", newDataImage)
             let result = data.results[0].urls.regular
            let des = data.results[0].alt_description

            //Gets the url & description of the query that was from the search result and display it.
            let dataImages = data.results.map(dt => {
                return {
                    url: dt.urls.regular,
                    des: dt.alt_description
                }
            })
            //

            if (match===false) {

                 result = check[JSON.parse(localStorage.getItem(currentIndex))]
                 des = checkDesc[JSON.parse(localStorage.getItem(currentIndex))]
                /*
                    Override the description & images of the search result with the new images&description from local storage
                    so that when the page refreshes, it will automatically display the background images and the image library of the current page that you are on
                    instead of the default image library & background.
                 */
                dataImages = newDataImage
             }
             //console.log("The match in fetchCity()",match)
             eleBody.style.backgroundImage = `url('${result}`
             eleDes.innerHTML = des
             totalPages = data.total_pages

            // console.log('my data ', dataImages)
            // console.log("input_result ==>",input_result)
            //Clear the imageContainer so that the everytime it displayImages, the image library will reflect the current 10
            //If you don't clear it, the imgContainer will contain to add every 10 images (array size will increase by 10 every time)
            imgContainer.innerHTML = ""
            displayImages(dataImages)
        })
}
const displayImages = (data) => {

    console.log("OMG",data)
    groupImages.length=0
    groupDescription.length =0
    for (let i=0; i< data.length; i++) {

        let resImage = document.createElement('img')
        resImage.src = data[i].url
        resImage.alt = data[i].des
        resImage.setAttribute(clickedIndex, i)
        resImage.classList.add('card')
         groupImages.push(data[i].url)
         groupDescription.push(data[i].des)

        imgContainer.append(resImage)
        resImage.addEventListener('click', imageSelection)
    }
    localStorage.setItem("groupOfImages",JSON.stringify(groupImages))
    localStorage.setItem("groupOfDescriptions",JSON.stringify(groupDescription))
   console.log("group of images ==>",groupImages)

}
const nextGroupOfImages = ()=>{

   match = true
    let a = (totalPages/10)
    pageNum++
    if(pageNum > a){
        pageNum = 1
    }
    fetchCity()
    console.log("Number =>",pageNum)
    localStorage.setItem(currentPage,pageNum)
    localStorage.setItem(currentIndex, JSON.stringify(0))
    //localStorage.setItem()
}
const prevGroupOfImages = ()=>{
   match = true
    pageNum--
    if (pageNum <= 1){
        pageNum = 1
    }
     fetchCity()
    localStorage.setItem(currentPage,pageNum)
    localStorage.setItem(currentIndex, JSON.stringify(0))
}

// Get the specific image first
// check if it was clicked, if so change the body of the background
const imageSelection = (event) =>{

   // Use event.target to get what you clicked that cause the event
     //match = true;
    console.log("match is=>",match)
    // groupImages.length=0
    let newUrl=event.target.src
    let newDesc=event.target.alt

    //console.log("myIndex =>",newDesc)
    let index = event.target.getAttribute(clickedIndex)
      //groupImages.push(index)
    //console.log("Index =>", newUrl)
    console.log("groupImages",groupImages)

    // Set the background to whatever we clicked through the index
    eleBody.style.backgroundImage = 'url('+newUrl
    eleDes.innerHTML = newDesc

    //fetchCity()
    localStorage.setItem(currentIndex, JSON.stringify(index))
    localStorage.setItem(currentImage, JSON.stringify(newUrl))
    localStorage.setItem("description", JSON.stringify(newDesc))

}
eleSearch.addEventListener('click', searchFun)
eleInput.addEventListener('keydown', inputSearch)
arrowRight.addEventListener('click',nextGroupOfImages)
arrowLeft.addEventListener('click',prevGroupOfImages)
window.addEventListener('load', fetchCity)